/*
Navicat MySQL Data Transfer

Source Server         : aliyun
Source Server Version : 50732
Source Host           : 47.93.225.64:3306
Source Database       : tsy_admin

Target Server Type    : MYSQL
Target Server Version : 50732
File Encoding         : 65001

Date: 2021-01-29 15:04:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `QRTZ_BLOB_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_CALENDARS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_CRON_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for `QRTZ_FIRED_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_JOB_DETAILS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', null, 'io.renren.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000176BFB6A9E07874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672656E72656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for `QRTZ_LOCKS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
INSERT INTO `QRTZ_LOCKS` VALUES ('RenrenScheduler', 'STATE_ACCESS');
INSERT INTO `QRTZ_LOCKS` VALUES ('RenrenScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `QRTZ_PAUSED_TRIGGER_GRPS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_SCHEDULER_STATE`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------
INSERT INTO `QRTZ_SCHEDULER_STATE` VALUES ('RenrenScheduler', 'DESKTOP-LRRHEEO1611895076848', '1611896315251', '15000');

-- ----------------------------
-- Table structure for `QRTZ_SIMPLE_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_SIMPROP_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `QRTZ_JOB_DETAILS` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', null, '1611896400000', '-1', '5', 'WAITING', 'CRON', '1609576895000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000176BFB6A9E07874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672656E72656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for `schedule_job`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1', 'testTask', 'renren', '0 0/30 * * * ?', '0', '参数测试', '2021-01-02 04:49:48');

-- ----------------------------
-- Table structure for `schedule_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES ('1', '1', 'testTask', 'renren', '0', null, '1', '2021-01-20 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('2', '1', 'testTask', 'renren', '0', null, '1', '2021-01-20 12:00:01');
INSERT INTO `schedule_job_log` VALUES ('3', '1', 'testTask', 'renren', '0', null, '0', '2021-01-20 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4', '1', 'testTask', 'renren', '0', null, '1', '2021-01-20 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('5', '1', 'testTask', 'renren', '0', null, '1', '2021-01-20 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('6', '1', 'testTask', 'renren', '0', null, '1', '2021-01-20 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('7', '1', 'testTask', 'renren', '0', null, '1', '2021-01-20 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('8', '1', 'testTask', 'renren', '0', null, '0', '2021-01-20 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('9', '1', 'testTask', 'renren', '0', null, '0', '2021-01-21 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('10', '1', 'testTask', 'renren', '0', null, '1', '2021-01-21 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('11', '1', 'testTask', 'renren', '0', null, '0', '2021-01-21 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('12', '1', 'testTask', 'renren', '0', null, '1', '2021-01-22 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('13', '1', 'testTask', 'renren', '0', null, '0', '2021-01-22 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('14', '1', 'testTask', 'renren', '0', null, '1', '2021-01-22 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('15', '1', 'testTask', 'renren', '0', null, '1', '2021-01-22 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('16', '1', 'testTask', 'renren', '0', null, '1', '2021-01-22 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('17', '1', 'testTask', 'renren', '0', null, '2', '2021-01-22 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('18', '1', 'testTask', 'renren', '0', null, '1', '2021-01-22 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('19', '1', 'testTask', 'renren', '0', null, '1', '2021-01-22 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('20', '1', 'testTask', 'renren', '0', null, '1', '2021-01-23 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('21', '1', 'testTask', 'renren', '0', null, '0', '2021-01-24 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('22', '1', 'testTask', 'renren', '0', null, '0', '2021-01-24 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('23', '1', 'testTask', 'renren', '0', null, '1', '2021-01-24 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('24', '1', 'testTask', 'renren', '0', null, '0', '2021-01-24 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('25', '1', 'testTask', 'renren', '0', null, '0', '2021-01-24 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('26', '1', 'testTask', 'renren', '0', null, '0', '2021-01-24 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('27', '1', 'testTask', 'renren', '0', null, '1', '2021-01-25 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('28', '1', 'testTask', 'renren', '0', null, '1', '2021-01-25 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('29', '1', 'testTask', 'renren', '0', null, '0', '2021-01-25 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('30', '1', 'testTask', 'renren', '0', null, '0', '2021-01-25 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('31', '1', 'testTask', 'renren', '0', null, '0', '2021-01-26 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('32', '1', 'testTask', 'renren', '0', null, '0', '2021-01-26 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('33', '1', 'testTask', 'renren', '0', null, '1', '2021-01-26 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('34', '1', 'testTask', 'renren', '0', null, '0', '2021-01-27 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('35', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('36', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('37', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('38', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('39', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('40', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('41', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('42', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('43', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('44', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('45', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('46', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('47', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('48', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('49', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('50', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('51', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('52', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('53', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('54', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('55', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('56', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('57', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('58', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('59', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('60', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('61', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('62', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('63', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('64', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('65', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('66', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('67', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('68', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('69', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('70', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('71', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('72', '1', 'testTask', 'renren', '0', null, '1', '2021-01-28 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('73', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('74', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('75', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('76', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('77', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('78', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('79', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('80', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('81', '1', 'testTask', 'renren', '0', null, '0', '2021-01-28 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('82', '1', 'testTask', 'renren', '0', null, '0', '2021-01-29 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('83', '1', 'testTask', 'renren', '0', null, '0', '2021-01-29 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('84', '1', 'testTask', 'renren', '0', null, '1', '2021-01-29 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('85', '1', 'testTask', 'renren', '0', null, '0', '2021-01-29 01:30:00');

-- ----------------------------
-- Table structure for `sys_captcha`
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统验证码';

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------
INSERT INTO `sys_captcha` VALUES ('02802b48-d7a7-4947-8cbd-21dccfef8643', '672cn', '2021-01-21 23:16:13');
INSERT INTO `sys_captcha` VALUES ('02f5e268-095d-4775-808b-2eb62129e800', 'enefy', '2021-01-21 12:01:37');
INSERT INTO `sys_captcha` VALUES ('166b76e1-6c89-4d5b-8f6d-1a275b59dfb7', 'xb3xx', '2021-01-24 13:26:36');
INSERT INTO `sys_captcha` VALUES ('2c4b0b41-5891-4304-8fdb-789402a0a200', 'x7dng', '2021-01-20 11:12:21');
INSERT INTO `sys_captcha` VALUES ('2f1b3a08-cee9-4384-8697-62dd317e6618', '84nfn', '2021-01-27 17:01:08');
INSERT INTO `sys_captcha` VALUES ('328640dd-773e-4159-8ac2-bc8f9832b866', '46x3b', '2021-01-28 16:06:00');
INSERT INTO `sys_captcha` VALUES ('3811762f-8735-4703-844e-04c4afa57335', 'angyg', '2021-01-21 11:50:21');
INSERT INTO `sys_captcha` VALUES ('51b3eec0-1440-46a7-8c19-6df128c71bc4', 'me72x', '2021-01-24 14:24:20');
INSERT INTO `sys_captcha` VALUES ('5ce1921a-28d5-4539-84b8-c048c48073eb', 'wpca6', '2021-01-02 15:13:50');
INSERT INTO `sys_captcha` VALUES ('5dc564ac-526b-43c7-8e2f-6c1ad8cd0a37', 'y8fd6', '2021-01-20 11:11:47');
INSERT INTO `sys_captcha` VALUES ('5ff9c87b-49aa-4e4b-81a5-1c69f85e85fb', '4adfg', '2021-01-25 14:33:47');
INSERT INTO `sys_captcha` VALUES ('70be9ad8-6003-41a5-8a77-f7fd23365ef2', 'e427f', '2021-01-21 11:49:34');
INSERT INTO `sys_captcha` VALUES ('7591a68d-fdac-4456-8953-c48266df0946', 'cb387', '2021-01-28 16:08:13');
INSERT INTO `sys_captcha` VALUES ('7bd61611-a717-4f0f-8f3f-16317d361f36', '7nncn', '2021-01-20 11:09:50');
INSERT INTO `sys_captcha` VALUES ('9224fd1a-7597-4c18-8e46-60b68314b666', '427n5', '2021-01-11 17:31:40');
INSERT INTO `sys_captcha` VALUES ('9eff5517-5f60-4043-81fe-6d0a6ad9001d', 'bpb3x', '2021-01-21 11:49:09');
INSERT INTO `sys_captcha` VALUES ('c4705614-755a-45a1-8a6c-ad8e0280f48c', 'xgww4', '2021-01-21 11:29:55');
INSERT INTO `sys_captcha` VALUES ('dbb022e2-b985-4675-8f4a-813683f1980c', '7n2mp', '2021-01-21 14:20:16');
INSERT INTO `sys_captcha` VALUES ('dc420f4b-99e3-4fba-87fe-9a3b05a3b08c', 'an6pn', '2021-01-28 00:46:41');
INSERT INTO `sys_captcha` VALUES ('e70561df-ff14-44a7-88d5-8779a4c39a1c', '5y28g', '2021-01-21 11:32:51');
INSERT INTO `sys_captcha` VALUES ('ec68db28-f5d4-4fa8-8ba2-4ab6a4daafe0', '7yx47', '2021-01-21 11:50:44');
INSERT INTO `sys_captcha` VALUES ('f13ba571-e9c6-41fc-8936-eb4613d0bfa2', '7pww5', '2021-01-28 16:07:51');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `param_key` (`param_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '铁路集团', '0', '0');
INSERT INTO `sys_dept` VALUES ('2', '1', '北京铁路局', '1', '0');
INSERT INTO `sys_dept` VALUES ('3', '1', '上海铁路局', '2', '0');
INSERT INTO `sys_dept` VALUES ('4', '3', '上海铁路局技术部', '0', '0');
INSERT INTO `sys_dept` VALUES ('5', '3', '销售部', '1', '-1');
INSERT INTO `sys_dept` VALUES ('6', '1', '西安铁路局', '1', '0');
INSERT INTO `sys_dept` VALUES ('7', '0', '铁路总局', '1', '0');
INSERT INTO `sys_dept` VALUES ('9', '0', '铁路总局', '1', '-1');
INSERT INTO `sys_dept` VALUES ('10', '0', '铁路总局', '1', '-1');

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '字典名称',
  `type` varchar(100) NOT NULL COMMENT '字典类型',
  `code` varchar(100) NOT NULL COMMENT '字典码',
  `value` varchar(1000) NOT NULL COMMENT '字典值',
  `order_num` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='数据字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '性别', 'sex', '0', '女', '0', null, '0');
INSERT INTO `sys_dict` VALUES ('2', '性别', 'sex', '1', '男', '1', null, '0');
INSERT INTO `sys_dict` VALUES ('3', '性别', 'sex', '2', '未知', '3', null, '0');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '{\"roleId\":1,\"roleName\":\"一级管理员\",\"deptId\":4,\"deptName\":\"技术部\",\"menuIdList\":[1,2,15,16,17,18,3,19,22],\"deptIdList\":[3,5],\"createTime\":\"Jan 2, 2021 4:44:31 PM\"}', '773', '0:0:0:0:0:0:0:1', '2021-01-02 16:44:32');
INSERT INTO `sys_log` VALUES ('2', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":95,\"parentId\":1,\"name\":\"部门管理\",\"url\":\"sys/dept\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]', '130', '0:0:0:0:0:0:0:1', '2021-01-21 10:26:49');
INSERT INTO `sys_log` VALUES ('3', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[71]', '277', '0:0:0:0:0:0:0:1', '2021-01-21 10:51:17');
INSERT INTO `sys_log` VALUES ('4', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[70]', '252', '0:0:0:0:0:0:0:1', '2021-01-21 10:51:28');
INSERT INTO `sys_log` VALUES ('5', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[69]', '254', '0:0:0:0:0:0:0:1', '2021-01-21 10:51:38');
INSERT INTO `sys_log` VALUES ('6', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[68]', '250', '0:0:0:0:0:0:0:1', '2021-01-21 10:51:48');
INSERT INTO `sys_log` VALUES ('7', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[67]', '204', '0:0:0:0:0:0:0:1', '2021-01-21 10:51:57');
INSERT INTO `sys_log` VALUES ('8', 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":4,\"roleName\":\"二级管理员\",\"remark\":\"xxx\",\"deptId\":4,\"menuIdList\":[61,82,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,77,78,79,80,81,83,84,85,86,87,88,89,-666666],\"createTime\":\"Jan 21, 2021 11:01:54 AM\"}]', '4424', '0:0:0:0:0:0:0:1', '2021-01-21 11:01:59');
INSERT INTO `sys_log` VALUES ('9', 'admin', '删除角色', 'io.renren.modules.sys.controller.SysRoleController.delete()', '[[4]]', '518', '0:0:0:0:0:0:0:1', '2021-01-21 11:10:28');
INSERT INTO `sys_log` VALUES ('10', 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":5,\"roleName\":\"二级管理员\",\"remark\":\"2\",\"deptId\":4,\"menuIdList\":[6,7,8,9,10,11,12,13,14,27,29,30,95,96,97,98,99,61,82,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,77,78,79,80,81,83,84,85,86,87,88,89,-666666,1],\"createTime\":\"Jan 21, 2021 11:13:08 AM\"}]', '5785', '0:0:0:0:0:0:0:1', '2021-01-21 11:13:14');
INSERT INTO `sys_log` VALUES ('11', 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":2,\"username\":\"zc\",\"password\":\"c8885cfc9ae94d9c7e5123fee31ad01ae88bbfd5aa9563f10cdf0209ad735c4d\",\"salt\":\"rHICpYMgagRLmr8zgCgN\",\"email\":\"1157440840@qq.com\",\"mobile\":\"18795970015\",\"status\":1,\"roleIdList\":[5],\"createTime\":\"Jan 21, 2021 11:24:06 AM\",\"deptId\":4}]', '551', '0:0:0:0:0:0:0:1', '2021-01-21 11:24:07');
INSERT INTO `sys_log` VALUES ('12', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":2,\"parentId\":1,\"name\":\"用户列表\",\"url\":\"sys/user\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]', '124', '0:0:0:0:0:0:0:1', '2021-01-21 23:18:34');
INSERT INTO `sys_log` VALUES ('13', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":2,\"parentId\":1,\"name\":\"用户列表\",\"url\":\"sys/user\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]', '152', '0:0:0:0:0:0:0:1', '2021-01-21 23:19:51');
INSERT INTO `sys_log` VALUES ('14', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":2,\"parentId\":1,\"name\":\"用户列表\",\"url\":\"sys/user\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]', '124', '0:0:0:0:0:0:0:1', '2021-01-21 23:27:30');
INSERT INTO `sys_log` VALUES ('15', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":2,\"parentId\":1,\"name\":\"管理员列表\",\"url\":\"sys/user\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]', '121', '0:0:0:0:0:0:0:1', '2021-01-21 23:27:47');
INSERT INTO `sys_log` VALUES ('16', 'admin', '删除项目', 'com.tsy.moniter.project.controller.ProjectController.delete()', '[[1344409139643781121]]', '126', '0:0:0:0:0:0:0:1', '2021-01-22 14:37:05');
INSERT INTO `sys_log` VALUES ('17', 'admin', '保存项目', 'com.tsy.moniter.project.controller.ProjectController.save()', '[{\"projectId\":1353230624147763202,\"projectName\":\"项目1\",\"deptId\":2,\"projectStatus\":\"进行中\",\"projectLocation\":\"辽宁旅顺\"}]', '122', '0:0:0:0:0:0:0:1', '2021-01-24 14:38:17');
INSERT INTO `sys_log` VALUES ('18', 'admin', '保存项目', 'com.tsy.moniter.project.controller.ProjectController.save()', '[{\"projectId\":1353237205321003010,\"projectName\":\"项目1\",\"deptId\":7,\"deptName\":\"铁路总局\",\"projectStatus\":\"进行中\",\"projectLocation\":\"山西晋城\"}]', '138', '0:0:0:0:0:0:0:1', '2021-01-24 15:04:27');
INSERT INTO `sys_log` VALUES ('19', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":100,\"parentId\":0,\"name\":\"组织结构管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"zhedie\",\"orderNum\":0}]', '99', '0:0:0:0:0:0:0:1', '2021-01-24 15:14:49');
INSERT INTO `sys_log` VALUES ('20', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":95,\"parentId\":100,\"name\":\"部门管理\",\"url\":\"sys/dept\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]', '129', '0:0:0:0:0:0:0:1', '2021-01-24 15:15:15');
INSERT INTO `sys_log` VALUES ('21', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":2,\"parentId\":100,\"name\":\"管理员列表\",\"url\":\"sys/user\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]', '129', '0:0:0:0:0:0:0:1', '2021-01-24 15:15:34');
INSERT INTO `sys_log` VALUES ('22', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":3,\"parentId\":100,\"name\":\"角色管理\",\"url\":\"sys/role\",\"type\":1,\"icon\":\"role\",\"orderNum\":2}]', '128', '0:0:0:0:0:0:0:1', '2021-01-24 15:15:48');
INSERT INTO `sys_log` VALUES ('23', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":61,\"parentId\":0,\"name\":\"监测项目管理\",\"url\":\"project/project\",\"type\":0,\"icon\":\"mudedi\",\"orderNum\":0}]', '96', '0:0:0:0:0:0:0:1', '2021-01-24 15:16:13');
INSERT INTO `sys_log` VALUES ('24', 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":5,\"roleName\":\"二级管理员\",\"remark\":\"2\",\"menuIdList\":[6,7,8,9,10,11,12,13,14,27,29,30,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,77,78,79,80,81,83,84,85,86,87,88,89,95,96,97,98,99,-666666,1,61,82,100]}]', '6038', '0:0:0:0:0:0:0:1', '2021-01-25 12:37:09');
INSERT INTO `sys_log` VALUES ('25', 'admin', '保存项目', 'com.tsy.moniter.project.controller.ProjectController.save()', '[{\"projectId\":1353565909800718337,\"projectName\":\"项目2\",\"deptId\":1,\"deptName\":\"铁路集团\",\"projectStatus\":\"进行中\",\"projectLocation\":\"河北唐山\"}]', '134', '0:0:0:0:0:0:0:1', '2021-01-25 12:50:36');
INSERT INTO `sys_log` VALUES ('26', 'admin', '保存项目', 'com.tsy.moniter.project.controller.ProjectController.save()', '[{\"projectId\":1353566007351840770,\"projectName\":\"项目3\",\"deptId\":2,\"deptName\":\"北京铁路局\",\"projectStatus\":\"暂停\",\"projectLocation\":\"直辖市北京\"}]', '131', '0:0:0:0:0:0:0:1', '2021-01-25 12:50:59');
INSERT INTO `sys_log` VALUES ('27', 'admin', '保存项目', 'com.tsy.moniter.project.controller.ProjectController.save()', '[{\"projectId\":1353566171617562626,\"projectName\":\"项目4\",\"deptId\":3,\"deptName\":\"上海铁路局\",\"projectStatus\":\"进行中\",\"projectLocation\":\"直辖市上海\"}]', '130', '0:0:0:0:0:0:0:1', '2021-01-25 12:51:38');
INSERT INTO `sys_log` VALUES ('28', 'admin', '保存项目', 'com.tsy.moniter.project.controller.ProjectController.save()', '[{\"projectId\":1353566232539828225,\"projectName\":\"项目5\",\"deptId\":4,\"deptName\":\"技术部\",\"projectStatus\":\"进行中\",\"projectLocation\":\"直辖市上海\"}]', '130', '0:0:0:0:0:0:0:1', '2021-01-25 12:51:52');
INSERT INTO `sys_log` VALUES ('29', 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":5,\"roleName\":\"二级管理员\",\"remark\":\"2\",\"menuIdList\":[6,7,8,9,10,11,12,13,14,27,29,30,61,82,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,77,78,79,80,81,83,84,85,86,87,88,89,95,96,97,98,99,-666666,1,100]}]', '5478', '0:0:0:0:0:0:0:1', '2021-01-25 14:28:27');
INSERT INTO `sys_log` VALUES ('30', 'admin', '保存监测体', 'com.tsy.moniter.project.controller.MbodyController.save()', '[{\"mbodyId\":1354067031565123585,\"mbodyName\":\"监测体1\",\"mbodyType\":\"类型1\",\"mbodyStatus\":\"正常\",\"mbodyIsSection\":0,\"mbodyBegin\":1000,\"mbodyEnd\":2000}]', '151', '0:0:0:0:0:0:0:1', '2021-01-26 22:01:52');
INSERT INTO `sys_log` VALUES ('31', 'admin', '保存监测点', 'com.tsy.moniter.project.controller.MpointController.save()', '[{\"mpointId\":1354069028607475714,\"mpointBegin\":10,\"mpointEnd\":100,\"mpointType\":1,\"mpointIsSection\":0}]', '136', '0:0:0:0:0:0:0:1', '2021-01-26 22:09:48');
INSERT INTO `sys_log` VALUES ('32', 'admin', '更新监测点', 'com.tsy.moniter.project.controller.MpointController.update()', '[{\"mpointId\":1354069028607475714,\"mpointBegin\":10,\"mpointEnd\":100,\"mpointType\":1,\"mpointIsSection\":0,\"mbodyId\":1354067031565123585}]', '180', '0:0:0:0:0:0:0:1', '2021-01-26 22:12:34');
INSERT INTO `sys_log` VALUES ('33', 'admin', '更新监测点', 'com.tsy.moniter.project.controller.MpointController.update()', '[{\"mpointId\":1354069028607475714,\"mpointBegin\":10,\"mpointEnd\":100,\"mpointType\":1,\"mpointIsSection\":0,\"mbodyId\":1354067031565123585}]', '239', '0:0:0:0:0:0:0:1', '2021-01-26 22:17:47');
INSERT INTO `sys_log` VALUES ('34', 'admin', '更新监测点', 'com.tsy.moniter.project.controller.MpointController.update()', '[{\"mpointId\":1354069028607475714,\"mpointBegin\":10,\"mpointEnd\":100,\"mpointType\":1,\"mpointIsSection\":0,\"mbodyId\":1354067031565123585}]', '241', '0:0:0:0:0:0:0:1', '2021-01-26 22:18:03');
INSERT INTO `sys_log` VALUES ('35', 'admin', '更新监测体', 'com.tsy.moniter.project.controller.MbodyController.update()', '[{\"mbodyId\":1354067031565123585,\"mbodyName\":\"监测体1\",\"mbodyType\":\"类型1\",\"mbodyStatus\":\"正常\",\"mbodyIsSection\":0,\"mbodyBegin\":1000,\"mbodyEnd\":2000}]', '123', '0:0:0:0:0:0:0:1', '2021-01-26 22:23:38');
INSERT INTO `sys_log` VALUES ('36', 'admin', '更新监测体', 'com.tsy.moniter.project.controller.MbodyController.update()', '[{\"mbodyId\":1354067031565123585,\"mbodyName\":\"监测体1\",\"mbodyType\":\"类型1\",\"mbodyStatus\":\"正常\",\"mbodyIsSection\":0,\"mbodyBegin\":1000,\"mbodyEnd\":2000,\"projectId\":1353566007351840770}]', '121', '0:0:0:0:0:0:0:1', '2021-01-26 22:25:31');
INSERT INTO `sys_log` VALUES ('37', 'admin', '更新监测点', 'com.tsy.moniter.project.controller.MpointController.update()', '[{\"mpointId\":1354069028607475714,\"mpointBegin\":10,\"mpointEnd\":100,\"mpointType\":1,\"mpointIsSection\":0,\"mbodyId\":1354067031565123585,\"projectId\":1353566007351840770}]', '163', '0:0:0:0:0:0:0:1', '2021-01-26 22:25:43');
INSERT INTO `sys_log` VALUES ('38', 'admin', '保存监测域', 'com.tsy.moniter.project.controller.MareaController.save()', '[{\"mareaId\":1354076079433748482,\"mareaAlertLevel\":1,\"mareaBegin\":11,\"mareaEnd\":22,\"projectId\":1353566171617562626}]', '177', '0:0:0:0:0:0:0:1', '2021-01-26 22:37:50');
INSERT INTO `sys_log` VALUES ('39', 'admin', '删除监测体', 'com.tsy.moniter.project.controller.MbodyController.delete()', '[[1354067031565123585]]', '247', '0:0:0:0:0:0:0:1', '2021-01-26 22:41:31');
INSERT INTO `sys_log` VALUES ('40', 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":6,\"roleName\":\"观察员\",\"remark\":\"\",\"menuIdList\":[32,37,42,47,52,57,78,85,15,19,96,-666666,61,82,31,36,41,46,51,56,77,83,84,100,2,3,95],\"createTime\":\"Jan 26, 2021 10:51:58 PM\"}]', '3347', '0:0:0:0:0:0:0:1', '2021-01-26 22:52:02');
INSERT INTO `sys_log` VALUES ('41', 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":3,\"username\":\"guancha\",\"password\":\"0887bebba63020e9f71ed2b98577309f69f38a2c4845920b3a29411f785f5d57\",\"salt\":\"pz2KgCTFeMXwr69rlccS\",\"email\":\"1157440840@qq.com\",\"mobile\":\"11111111111\",\"status\":1,\"roleIdList\":[6],\"createTime\":\"Jan 26, 2021 10:53:07 PM\",\"deptId\":2}]', '626', '0:0:0:0:0:0:0:1', '2021-01-26 22:53:08');
INSERT INTO `sys_log` VALUES ('42', 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":3,\"username\":\"guancha\",\"salt\":\"pz2KgCTFeMXwr69rlccS\",\"email\":\"1157440840@qq.com\",\"mobile\":\"11111111111\",\"status\":1,\"roleIdList\":[6],\"deptId\":3}]', '536', '0:0:0:0:0:0:0:1', '2021-01-26 22:58:35');
INSERT INTO `sys_log` VALUES ('43', 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":6,\"roleName\":\"观察员\",\"remark\":\"\",\"menuIdList\":[14,32,37,42,47,52,57,78,85,15,19,96,-666666,1,6,61,82,31,36,41,46,51,56,77,83,84,100,2,3,95]}]', '3557', '0:0:0:0:0:0:0:1', '2021-01-26 23:07:25');
INSERT INTO `sys_log` VALUES ('44', 'zc', '保存监测域与监测点的关系', 'com.tsy.moniter.project.controller.AreaPointController.save()', '[{\"mareaId\":1354078261956579330,\"mpointId\":1354077724959887361,\"apId\":1354353438481219586,\"projectId\":1353566232539828225}]', '282', '0:0:0:0:0:0:0:1', '2021-01-27 16:59:57');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'system', '0');
INSERT INTO `sys_menu` VALUES ('2', '100', '管理员列表', 'sys/user', null, '1', 'admin', '1');
INSERT INTO `sys_menu` VALUES ('3', '100', '角色管理', 'sys/role', null, '1', 'role', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'sys/menu', null, '1', 'menu', '3');
INSERT INTO `sys_menu` VALUES ('5', '1', 'SQL监控', 'http://localhost:8080/renren-fast/druid/sql.html', null, '1', 'sql', '4');
INSERT INTO `sys_menu` VALUES ('6', '1', '定时任务', 'job/schedule', null, '1', 'job', '5');
INSERT INTO `sys_menu` VALUES ('7', '6', '查看', null, 'sys:schedule:list,sys:schedule:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('8', '6', '新增', null, 'sys:schedule:save', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('9', '6', '修改', null, 'sys:schedule:update', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '6', '删除', null, 'sys:schedule:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '6', '暂停', null, 'sys:schedule:pause', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '6', '恢复', null, 'sys:schedule:resume', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('13', '6', '立即执行', null, 'sys:schedule:run', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '6', '日志列表', null, 'sys:schedule:log', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('27', '1', '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('29', '1', '系统日志', 'sys/log', 'sys:log:list', '1', 'log', '7');
INSERT INTO `sys_menu` VALUES ('30', '1', '文件上传', 'oss/oss', 'sys:oss:all', '1', 'oss', '6');
INSERT INTO `sys_menu` VALUES ('31', '82', '项目管理', 'project/project', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('32', '31', '查看', null, 'project:project:list,project:project:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('33', '31', '新增', null, 'project:project:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('34', '31', '修改', null, 'project:project:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('35', '31', '删除', null, 'project:project:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('36', '82', '监测域', 'project/marea', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('37', '36', '查看', null, 'project:marea:list,project:marea:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('38', '36', '新增', null, 'project:marea:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('39', '36', '修改', null, 'project:marea:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('40', '36', '删除', null, 'project:marea:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('41', '82', '监测体', 'project/mbody', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('42', '41', '查看', null, 'project:mbody:list,project:mbody:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('43', '41', '新增', null, 'project:mbody:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('44', '41', '修改', null, 'project:mbody:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('45', '41', '删除', null, 'project:mbody:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('46', '82', '监测体类型', 'project/mbodytype', null, '1', 'config', '7');
INSERT INTO `sys_menu` VALUES ('47', '46', '查看', null, 'project:mbodytype:list,project:mbodytype:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('48', '46', '新增', null, 'project:mbodytype:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('49', '46', '修改', null, 'project:mbodytype:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('50', '46', '删除', null, 'project:mbodytype:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('51', '82', '监测点', 'project/mpoint', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('52', '51', '查看', null, 'project:mpoint:list,project:mpoint:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('53', '51', '新增', null, 'project:mpoint:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('54', '51', '修改', null, 'project:mpoint:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('55', '51', '删除', null, 'project:mpoint:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('56', '82', '监测点类型', 'project/mpointtype', null, '1', 'config', '7');
INSERT INTO `sys_menu` VALUES ('57', '56', '查看', null, 'project:mpointtype:list,project:mpointtype:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('58', '56', '新增', null, 'project:mpointtype:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('59', '56', '修改', null, 'project:mpointtype:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('60', '56', '删除', null, 'project:mpointtype:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('61', '0', '监测项目管理', 'project/project', null, '0', 'mudedi', '0');
INSERT INTO `sys_menu` VALUES ('77', '82', '监测域与监测点关系配置', 'project/areapoint', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('78', '77', '查看', null, 'project:areapoint:list,project:areapoint:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('79', '77', '新增', null, 'project:areapoint:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('80', '77', '修改', null, 'project:areapoint:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('81', '77', '删除', null, 'project:areapoint:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('82', '61', '监测逻辑管理', '', '', '0', 'menu', '0');
INSERT INTO `sys_menu` VALUES ('83', '61', '设备配置管理', '', '', '0', 'shezhi', '0');
INSERT INTO `sys_menu` VALUES ('84', '83', '项目与设备配置', 'project/mdeviceconf', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('85', '84', '查看', null, 'project:mdeviceconf:list,project:mdeviceconf:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('86', '84', '新增', null, 'project:mdeviceconf:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('87', '84', '修改', null, 'project:mdeviceconf:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('88', '84', '删除', null, 'project:mdeviceconf:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('89', '61', '监测实况', 'project/live', '', '1', 'sousuo', '0');
INSERT INTO `sys_menu` VALUES ('95', '100', '部门管理', 'sys/dept', null, '1', 'admin', '1');
INSERT INTO `sys_menu` VALUES ('96', '95', '查看', null, 'sys:dept:list,sys:dept:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('97', '95', '新增', null, 'sys:dept:save,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('98', '95', '修改', null, 'sys:dept:update,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('99', '95', '删除', null, 'sys:dept:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('100', '0', '组织结构管理', '', '', '0', 'zhedie', '0');

-- ----------------------------
-- Table structure for `sys_oss`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Records of sys_oss
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '一级管理员', null, '2021-01-02 16:44:31');
INSERT INTO `sys_role` VALUES ('5', '二级管理员', '2', '2021-01-21 11:13:08');
INSERT INTO `sys_role` VALUES ('6', '观察员', '', '2021-01-26 22:51:59');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=utf8mb4 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('3', '1', '15');
INSERT INTO `sys_role_menu` VALUES ('4', '1', '16');
INSERT INTO `sys_role_menu` VALUES ('5', '1', '17');
INSERT INTO `sys_role_menu` VALUES ('6', '1', '18');
INSERT INTO `sys_role_menu` VALUES ('7', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('8', '1', '19');
INSERT INTO `sys_role_menu` VALUES ('9', '1', '22');
INSERT INTO `sys_role_menu` VALUES ('278', '5', '6');
INSERT INTO `sys_role_menu` VALUES ('279', '5', '7');
INSERT INTO `sys_role_menu` VALUES ('280', '5', '8');
INSERT INTO `sys_role_menu` VALUES ('281', '5', '9');
INSERT INTO `sys_role_menu` VALUES ('282', '5', '10');
INSERT INTO `sys_role_menu` VALUES ('283', '5', '11');
INSERT INTO `sys_role_menu` VALUES ('284', '5', '12');
INSERT INTO `sys_role_menu` VALUES ('285', '5', '13');
INSERT INTO `sys_role_menu` VALUES ('286', '5', '14');
INSERT INTO `sys_role_menu` VALUES ('287', '5', '27');
INSERT INTO `sys_role_menu` VALUES ('288', '5', '29');
INSERT INTO `sys_role_menu` VALUES ('289', '5', '30');
INSERT INTO `sys_role_menu` VALUES ('290', '5', '61');
INSERT INTO `sys_role_menu` VALUES ('291', '5', '82');
INSERT INTO `sys_role_menu` VALUES ('292', '5', '31');
INSERT INTO `sys_role_menu` VALUES ('293', '5', '32');
INSERT INTO `sys_role_menu` VALUES ('294', '5', '33');
INSERT INTO `sys_role_menu` VALUES ('295', '5', '34');
INSERT INTO `sys_role_menu` VALUES ('296', '5', '35');
INSERT INTO `sys_role_menu` VALUES ('297', '5', '36');
INSERT INTO `sys_role_menu` VALUES ('298', '5', '37');
INSERT INTO `sys_role_menu` VALUES ('299', '5', '38');
INSERT INTO `sys_role_menu` VALUES ('300', '5', '39');
INSERT INTO `sys_role_menu` VALUES ('301', '5', '40');
INSERT INTO `sys_role_menu` VALUES ('302', '5', '41');
INSERT INTO `sys_role_menu` VALUES ('303', '5', '42');
INSERT INTO `sys_role_menu` VALUES ('304', '5', '43');
INSERT INTO `sys_role_menu` VALUES ('305', '5', '44');
INSERT INTO `sys_role_menu` VALUES ('306', '5', '45');
INSERT INTO `sys_role_menu` VALUES ('307', '5', '46');
INSERT INTO `sys_role_menu` VALUES ('308', '5', '47');
INSERT INTO `sys_role_menu` VALUES ('309', '5', '48');
INSERT INTO `sys_role_menu` VALUES ('310', '5', '49');
INSERT INTO `sys_role_menu` VALUES ('311', '5', '50');
INSERT INTO `sys_role_menu` VALUES ('312', '5', '51');
INSERT INTO `sys_role_menu` VALUES ('313', '5', '52');
INSERT INTO `sys_role_menu` VALUES ('314', '5', '53');
INSERT INTO `sys_role_menu` VALUES ('315', '5', '54');
INSERT INTO `sys_role_menu` VALUES ('316', '5', '55');
INSERT INTO `sys_role_menu` VALUES ('317', '5', '56');
INSERT INTO `sys_role_menu` VALUES ('318', '5', '57');
INSERT INTO `sys_role_menu` VALUES ('319', '5', '58');
INSERT INTO `sys_role_menu` VALUES ('320', '5', '59');
INSERT INTO `sys_role_menu` VALUES ('321', '5', '60');
INSERT INTO `sys_role_menu` VALUES ('322', '5', '77');
INSERT INTO `sys_role_menu` VALUES ('323', '5', '78');
INSERT INTO `sys_role_menu` VALUES ('324', '5', '79');
INSERT INTO `sys_role_menu` VALUES ('325', '5', '80');
INSERT INTO `sys_role_menu` VALUES ('326', '5', '81');
INSERT INTO `sys_role_menu` VALUES ('327', '5', '83');
INSERT INTO `sys_role_menu` VALUES ('328', '5', '84');
INSERT INTO `sys_role_menu` VALUES ('329', '5', '85');
INSERT INTO `sys_role_menu` VALUES ('330', '5', '86');
INSERT INTO `sys_role_menu` VALUES ('331', '5', '87');
INSERT INTO `sys_role_menu` VALUES ('332', '5', '88');
INSERT INTO `sys_role_menu` VALUES ('333', '5', '89');
INSERT INTO `sys_role_menu` VALUES ('334', '5', '95');
INSERT INTO `sys_role_menu` VALUES ('335', '5', '96');
INSERT INTO `sys_role_menu` VALUES ('336', '5', '97');
INSERT INTO `sys_role_menu` VALUES ('337', '5', '98');
INSERT INTO `sys_role_menu` VALUES ('338', '5', '99');
INSERT INTO `sys_role_menu` VALUES ('339', '5', '-666666');
INSERT INTO `sys_role_menu` VALUES ('340', '5', '1');
INSERT INTO `sys_role_menu` VALUES ('341', '5', '100');
INSERT INTO `sys_role_menu` VALUES ('369', '6', '14');
INSERT INTO `sys_role_menu` VALUES ('370', '6', '32');
INSERT INTO `sys_role_menu` VALUES ('371', '6', '37');
INSERT INTO `sys_role_menu` VALUES ('372', '6', '42');
INSERT INTO `sys_role_menu` VALUES ('373', '6', '47');
INSERT INTO `sys_role_menu` VALUES ('374', '6', '52');
INSERT INTO `sys_role_menu` VALUES ('375', '6', '57');
INSERT INTO `sys_role_menu` VALUES ('376', '6', '78');
INSERT INTO `sys_role_menu` VALUES ('377', '6', '85');
INSERT INTO `sys_role_menu` VALUES ('378', '6', '15');
INSERT INTO `sys_role_menu` VALUES ('379', '6', '19');
INSERT INTO `sys_role_menu` VALUES ('380', '6', '96');
INSERT INTO `sys_role_menu` VALUES ('381', '6', '-666666');
INSERT INTO `sys_role_menu` VALUES ('382', '6', '1');
INSERT INTO `sys_role_menu` VALUES ('383', '6', '6');
INSERT INTO `sys_role_menu` VALUES ('384', '6', '61');
INSERT INTO `sys_role_menu` VALUES ('385', '6', '82');
INSERT INTO `sys_role_menu` VALUES ('386', '6', '31');
INSERT INTO `sys_role_menu` VALUES ('387', '6', '36');
INSERT INTO `sys_role_menu` VALUES ('388', '6', '41');
INSERT INTO `sys_role_menu` VALUES ('389', '6', '46');
INSERT INTO `sys_role_menu` VALUES ('390', '6', '51');
INSERT INTO `sys_role_menu` VALUES ('391', '6', '56');
INSERT INTO `sys_role_menu` VALUES ('392', '6', '77');
INSERT INTO `sys_role_menu` VALUES ('393', '6', '83');
INSERT INTO `sys_role_menu` VALUES ('394', '6', '84');
INSERT INTO `sys_role_menu` VALUES ('395', '6', '100');
INSERT INTO `sys_role_menu` VALUES ('396', '6', '2');
INSERT INTO `sys_role_menu` VALUES ('397', '6', '3');
INSERT INTO `sys_role_menu` VALUES ('398', '6', '95');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e1153123d7d180ceeb820d577ff119876678732a68eef4e6ffc0b1f06a01f91b', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', '1', '1', '2016-11-11 11:11:11');
INSERT INTO `sys_user` VALUES ('2', 'zc', 'c8885cfc9ae94d9c7e5123fee31ad01ae88bbfd5aa9563f10cdf0209ad735c4d', 'rHICpYMgagRLmr8zgCgN', '1157440840@qq.com', '18795970015', '1', '4', '2021-01-21 11:24:06');
INSERT INTO `sys_user` VALUES ('3', 'guancha', '0887bebba63020e9f71ed2b98577309f69f38a2c4845920b3a29411f785f5d57', 'pz2KgCTFeMXwr69rlccS', '1157440840@qq.com', '11111111111', '1', '3', '2021-01-26 22:53:08');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '2', '5');
INSERT INTO `sys_user_role` VALUES ('3', '3', '6');

-- ----------------------------
-- Table structure for `sys_user_token`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `token` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统用户Token';

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES ('1', '5b9da7618dec5a724f2d2b7de5f11574', '2021-01-29 04:04:10', '2021-01-28 16:04:10');
INSERT INTO `sys_user_token` VALUES ('2', '93048b65673069c4c2103b786fafcce3', '2021-01-28 12:41:49', '2021-01-28 00:41:49');
INSERT INTO `sys_user_token` VALUES ('3', '5eef10111b25fe47bb314e7ca06a6c3b', '2021-01-27 10:59:24', '2021-01-26 22:59:24');
